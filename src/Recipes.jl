module RecipeBook
export Recipe, find_matches

using Gtk

mutable struct RecipeBook
    author::String
    recipes::Dict{String, Dict{String, Recipe}}
end

struct Recipe
    ingredients::Vector{String}
    instruction::String
    tags::Vector{String}
end

function add_recipe!(book; name, category, ingredients, instruction, tags)
    book.recipes[category][name] = Recipe(ingredients, instruction, tags)
end

function find_matches(book, ingredients)
    matches = []
    for (index, recepie) in enumerate(book)
        for ingredient in ingredients
            if ingredient in recepie.ingredients
                push!(matches, index)
                break
            end
        end
    end
    return book[matches]
end

function parse_recipes(recepie)
    header = "\\section{$(recepie.name)}"
    instruction = recepie.instruction
    println("""
            \\begin{document}
            $header
            \\par
            $instruction
            \\end{document}""")
end

function add_recipe!(book, name, ingredients, instruction, category, tags)
    r = Recipe(name, ingredients, instruction, category, tags)
    push!(book, r)
end

end


using Random
knoedel = RecipeBook.Recipe("Knoedel", ["Knoedelbrot", "Milch", "Kaese"], "Mischen, kochen, fertig", Hauptspeise, [])

book = [knoedel,]
for i in 1:10
    push!(book, RecipeBook.Recipe(randstring(9), [randstring(3) for _ in 1:4], randstring(5), rand(instances(Categories)),[randstring(4) for _ in 1:3]))
end
#matches = findall(x -> any(any(x.ingredients .== ingredient) for ingredient in ingredients), book)
#println.(book[matches])

